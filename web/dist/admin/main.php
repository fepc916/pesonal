<body class="fixed-sn light-blue-skin" ng-app="myApp">

    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-4 fixed">
          <ul class="custom-scrollbar">
              <!--/.Search Form-->
              <!-- Side navigation links -->
              <li>
                  <ul class="collapsible collapsible-accordion">
                      <li><a class="collapsible-header waves-effect arrow-r" href="#!/"> About Me</a></li>
                      <li><a class="collapsible-header waves-effect arrow-r" href="#!/myskills"> My Skills</a></li>
                      <li><a class="collapsible-header waves-effect arrow-r" href="#!/myexperience"> My Experience</a></li>
                      <li><a class="collapsible-header waves-effect arrow-r" href="#!/myprojects"> My Projects</a></li>
                      <li><a class="collapsible-header waves-effect arrow-r" href="#!/mycertifications"> My Certifications</a></li>
                  </ul>
              </li>
              <!--/. Side navigation links -->
          </ul>
          <div class="sidenav-bg mask-strong"></div>
        </div>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
            <!-- SideNav slide-out button -->
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
            </div>
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn mr-auto">
                <p>Admin Panel Portfolio</p>
            </div>
            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="?ses_stop">Exit</a>
                </li>
            </ul>
        </nav>
        <!-- /.Navbar -->
    </header>
    <!--/.Double navigation-->
    
    <!--Main Layout-->
    <main ng-view>

    </main>