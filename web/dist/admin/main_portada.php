<div class="container-fluid mt-5 text-center ng-scope">
    <h1>About Me</h1>
    <br>
    <form class="md-form ng-pristine ng-valid platform" method="POST" name="portada" id="portada" enctype="multipart/form-data" action="func_portada.php">
    <div class="file-field">
    <div class="btn btn-primary btn-sm float-left">
    <span>Profile Image</span>
    <input type="file" name="profile" id="profile">
    </div>
    <div class="file-path-wrapper"> 
    <input class="file-path validate" name="profile" id="profile" type="text" placeholder="Upload your file">
    </div>
    </div>
    <br>
    <div class="file-field">
    <div class="btn btn-primary btn-sm float-left">
    <span>Background</span>
    <input type="file" name="background" id="background">
    </div>
    <div class="file-path-wrapper">
    <input class="file-path validate" name="background" id="background" type="text" placeholder="Upload your file">
    </div>
    </div>
    <div class="md-form input-group mb-3">
    <div class="input-group-prepend">
    </div>
    <input type="text" class="form-control" placeholder="Your Name" name="name" id="name" aria-label="Username" aria-describedby="material-addon1">
    </div>
    <div class="md-form mb-4 pink-textarea active-pink-textarea">
    <textarea type="text" name="description" id="description" class="md-textarea form-control" rows="3"></textarea>
    <label for="form21">Who are you?</label>
    </div>
    <button class="btn blue-gradient" type="submit">Submit Form</button>
    </form>
</div>