<?php
$servername = "localhost";
$username = "root";
$password = "admin";
$dbname = "portafolio";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


function upload_img($img) {
// Additional Functions
$target_dir = "img/";
$target_file = $target_dir . basename($_FILES[$img]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES[$img]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.\n";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.\n";
    $uploadOk = 0;
    exit;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "svg" ) {
    echo "Sorry, only JPG, JPEG, PNG, GIF & SVG files are allowed.\n";
    $uploadOk = 0;
    exit;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.\n";
    exit;
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES[$img]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES[$img]["name"]). " has been uploaded.\n";
    } else {
        echo "Sorry, there was an error uploading your file.\n";
    }
}
}
?>
