var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "main_portada.php"
    })
    .when("/myskills", {
        templateUrl : "main_content.php?type=skills"
    })
    .when("/myexperience", {
        templateUrl : "main_content.php?type=experience"
    })
    .when("/myprojects", {
        templateUrl : "main_content.php?type=projects"
    })
    .when("/mycertifications", {
        templateUrl : "main_content.php?type=certifications"
    });
});