function rm(type,id) {    
    $.ajax({url: 'func_remove.php?type='+type+'&id='+id, success: function(result){
        $("#body-modal").html(result);
        $('#myModal').modal('show');
        location.reload();
    }});
}

function mk(type) {
    $.ajax({url: 'func_make.php?type='+type, success: function(result){
        $("#body-modal").html(result);
        $('#myModal').modal('show');
    }});
}