<?php 
include('db.php');
error_reporting(0);
session_start();
?>
<?php
if(isset($_GET['ses_start'])) {
    $_SESSION['usr']=$_GET['ses_start'];
    header("Location: /admin");
}
if(isset($_GET['ses_stop'])) {
    session_destroy();
    header("Location: /admin");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel Portfolio</title>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://mdbootstrap.com/previews/docs/latest/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://mdbootstrap.com/previews/docs/latest/css/mdb.min.css" rel="stylesheet">
    <script type="text/javascript" src="app.js"></script>
    <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/popper.min.js"></script>
    <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="sendform.js"></script>
    <script type="text/javascript" src="functions.js"></script>
</head>
<?php include("auth.php"); ?>
    <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/mdb.min.js"></script>
    <script>
    $(".button-collapse").sideNav();
    </script>
<div class="container">
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div id="body-modal" class="modal-body">
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>