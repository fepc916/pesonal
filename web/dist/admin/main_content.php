<?php
$type = $_GET['type'];
$print = ucfirst(strtolower($type));
?>
<div class="container-fluid mt-5 text-center ng-scope">
    <h1>My <?php echo $print; ?></h1>
    <br>
    <form class="md-form ng-pristine ng-valid">
    <div class="table-responsive text-nowrap">

<table class="table">
  <thead>
    <tr>
      <?php if($type=="skills") { ?>
      <th scope="col">Lenguage</th>
      <?php } ?>
      <?php if($type=="experience") { ?>
      <th scope="col">Name</th>
      <?php } ?>
      <?php if($type=="projects") { ?>  
      <th scope="col">Title</th>
      <?php } ?>
      <?php if($type=="certifications") { ?>  
      <th scope="col">Certification</th>
      <?php } ?>
      <th scope="col">Remove</th>
    </tr>
  </thead>
  <tbody>
<?php include('db.php');$sql = sprintf("SELECT * FROM %s",$type);$result = mysqli_query($conn, $sql);if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
    <tr>
      <?php if($type=="skills") { ?>
      <td><?php echo $row["lenguaje"]; ?></td>
      <?php } ?>
      <?php if($type=="experience") { ?>
      <td><?php echo $row["nombre"]; ?></td>
      <?php } ?>
      <?php if($type=="projects") { ?>
      <td><?php echo $row["title"]; ?></td>
      <?php } ?>
      <?php if($type=="certifications") { ?>
      <td><?php echo $row["diploma"]; ?></td>
      <?php } ?>
      <td><a onclick='rm("<?php echo $type; ?>","<?php echo $row["id"]; ?>");'><i class="fa fa-remove" aria-hidden="true"></i></a></td>
    </tr>
<?php }} else {}mysqli_close($conn); ?>
  </tbody>
</table>
<button type="button" class="btn btn-primary btn-rounded" onclick="mk('<?php echo $type; ?>');">Add New Item</button>

</div>
    </form>
</div>