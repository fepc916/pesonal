<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
// Grupo de rutas para el API
$app->group('/v1', function () use ($app) {
    // Version group
    $app->group('/empresas', function () use ($app) {
      $app->get('/', 'obtenerEmpresas');
      $app->post('/crear', 'agregarEmpresa');
      $app->put('/actualizar/{id}', 'actualizarEmpresa');
      $app->delete('/eliminar/{id}', 'eliminarEmpresa');
    });
    $app->group('/portada', function () use ($app) {
        $app->get('/', 'obtenerPortada');
        $app->put('/actualizar', 'actualizarPortada');
      });
      $app->group('/skills', function () use ($app) {
        $app->get('/', 'obtenerSkills');
        $app->post('/crear', 'agregarSkills');
        $app->put('/actualizar/{id}', 'actualizarSkills');
        $app->delete('/eliminar/{id}', 'eliminarSkills');
      });
      $app->group('/trabajos', function () use ($app) {
        $app->get('/', 'obtenerTrabajos');
        $app->post('/crear', 'agregarTrabajos');
        $app->put('/actualizar/{id}', 'actualizarTrabajos');
        $app->delete('/eliminar/{id}', 'eliminarTrabajos');
      });
      $app->group('/certificaciones', function () use ($app) {
        $app->get('/', 'obtenerCertificacion');
        $app->post('/crear', 'agregarCertificacion');
        $app->put('/actualizar/{id}', 'actualizarCertificacion');
        $app->delete('/eliminar/{id}', 'eliminarCertificacion');
      });
      $app->group('/contacto', function () use ($app) {
        $app->get('/', 'obtenerContacto');
        $app->post('/crear', 'agregarContacto');
        $app->put('/actualizar/{id}', 'actualizarContacto');
        $app->delete('/eliminar/{id}', 'eliminarContacto');
      });
  });   
