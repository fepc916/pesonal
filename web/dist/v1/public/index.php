<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

function getConnection() {
    $dbhost="localhost";
    $dbuser="root";
    $dbpass="admin";
    $dbname="portafolio";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

function obtenerEmpresas($response) {
    $sql = "SELECT * FROM experience";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return json_encode($employees);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function agregarEmpresa($request) {
    $emp = json_decode($request->getBody());
    
    $sql = "INSERT INTO experience (nombre, duracion, cargo) VALUES (:nombre, :duracion, :cargo)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("nombre", $emp->nombre);
        $stmt->bindParam("duracion", $emp->duracion);
        $stmt->bindParam("cargo", $emp->cargo);
        $stmt->execute();
        $emp->id = $db->lastInsertId();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function actualizarEmpresa($request) {
    $emp = json_decode($request->getBody());
    $id = $request->getAttribute('id');
    $sql = "UPDATE experience SET nombre=:nombre, duracion=:duracion, cargo=:cargo WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("nombre", $emp->nombre);
        $stmt->bindParam("duracion", $emp->duracion);
        $stmt->bindParam("cargo", $emp->cargo);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function eliminarEmpresa($request) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM experience WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"error":{"text":"Borrado Exitoso"}}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}




function obtenerPortada($response) {
    $sql = "SELECT * FROM portada";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return str_replace(array("[","]"),"",json_encode($employees));
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function actualizarPortada($request) {
    $emp = json_decode($request->getBody());
    $sql = "UPDATE portada SET img=:img, fondo=:fondo, name=:name, description=:description WHERE id='1'";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("img", $emp->img);
        $stmt->bindParam("fondo", $emp->fondo);
        $stmt->bindParam("name", $emp->name);
        $stmt->bindParam("description", $emp->description);
        $stmt->execute();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}







function obtenerSkills($response) {
    $sql = "SELECT * FROM skills";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return json_encode($employees);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function agregarSkills($request) {
    $emp = json_decode($request->getBody());
    
    $sql = "INSERT INTO skills (lenguaje, porcentaje) VALUES (:lenguaje, :porcentaje)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("lenguaje", $emp->lenguaje);
        $stmt->bindParam("porcentaje", $emp->porcentaje);
        $stmt->execute();
        $emp->id = $db->lastInsertId();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function actualizarSkills($request) {
    $emp = json_decode($request->getBody());
    $id = $request->getAttribute('id');
    $sql = "UPDATE skills SET lenguaje=:lenguaje, porcentaje=:porcentaje WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("lenguaje", $emp->lenguaje);
        $stmt->bindParam("porcentaje", $emp->porcentaje);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function eliminarSkills($request) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM skills WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"error":{"text":"Borrado Exitoso"}}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}




function obtenerTrabajos($response) {
    $sql = "SELECT * FROM projects";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return json_encode($employees);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function agregarTrabajos($request) {
    $emp = json_decode($request->getBody());
    
    $sql = "INSERT INTO projects (img, title, description, empresaid) VALUES (:img, :title, :description, :empresaid)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("img", $emp->img);
        $stmt->bindParam("title", $emp->title);
        $stmt->bindParam("description", $emp->description);
        $stmt->bindParam("empresaid", $emp->empresaid);
        $stmt->execute();
        $emp->id = $db->lastInsertId();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function actualizarTrabajos($request) {
    $emp = json_decode($request->getBody());
    $id = $request->getAttribute('id');
    $sql = "UPDATE projects SET img=:img, title=:tile, description=:description, empresaid=:empresaid WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("img", $emp->img);
        $stmt->bindParam("title", $emp->title);
        $stmt->bindParam("description", $emp->description);
        $stmt->bindParam("empresaid", $emp->empresaid);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function eliminarTrabajos($request) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM projects WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"error":{"text":"Borrado Exitoso"}}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function obtenerContacto($response) {
    $sql = "SELECT * FROM contacto";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return str_replace(array("[","]"),"",json_encode($employees));
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function agregarContacto($request) {
    $emp = json_decode($request->getBody());
    
    $sql = "INSERT INTO contacto (icon, link, type) VALUES (:icon, :link, :type)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("icon", $emp->icon);
        $stmt->bindParam("link", $emp->link);
        $stmt->bindParam("type", $emp->type);
        $stmt->execute();
        $emp->id = $db->lastInsertId();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function actualizarContacto($request) {
    $emp = json_decode($request->getBody());
    $id = $request->getAttribute('id');
    $sql = "UPDATE contacto SET icon=:icon, link=:link, type=:type WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("icon", $emp->icon);
        $stmt->bindParam("link", $emp->link);
        $stmt->bindParam("type", $emp->type);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function eliminarContacto($request) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM contacto WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"error":{"text":"Borrado Exitoso"}}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}


function obtenerCertificacion($response) {
    $sql = "SELECT * FROM certifications";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return json_encode($employees);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function agregarCertificacion($request) {
    $emp = json_decode($request->getBody());
    
    $sql = "INSERT INTO certifications (fecha, diploma) VALUES (:fecha, :diploma)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("fecha", $emp->nombre);
        $stmt->bindParam("diploma", $emp->duracion);
        $stmt->execute();
        $emp->id = $db->lastInsertId();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function actualizarCertificacion($request) {
    $emp = json_decode($request->getBody());
    $id = $request->getAttribute('id');
    $sql = "UPDATE certifications SET fecha=:fecha, diploma=:diploma WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("fecha", $emp->nombre);
        $stmt->bindParam("diploma", $emp->duracion);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo json_encode($emp);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function eliminarCertificacion($request) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM certifications WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"error":{"text":"Borrado Exitoso"}}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}


$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new \CorsSlim\CorsSlim($corsOptions);

// Run app
$app->run();
