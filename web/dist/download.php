<?php
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="Resume.pdf"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize("Resume.pdf"));
        flush(); // Flush system output buffer
        readfile("Resume.pdf");
        exit;
?>