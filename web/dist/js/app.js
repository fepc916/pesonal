var app = angular.module("portfolio", ["ngRoute","ngAnimate","ngTouch"])


.controller('portada', function($scope, $http, $interval, $location, $window) {
    twoFunction();
    $interval(function () {
        $http.get('/v1/portada/').
        then(function(response) {
            $scope.portada = response.data;
        });
        if($window.location.pathname=="/") {
    $scope.swipeLeft = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-left ng-enter');
        $location.path("/MySkills");
    };
    $scope.swipeRight = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-right ng-enter');
        $location.path("/ContactMe");
    };
} if($window.location.pathname=="/MySkills") {
    $scope.swipeLeft = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-left ng-enter');
        $location.path("/MyExperience");
    };
    $scope.swipeRight = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-right ng-enter');
        $location.path("/");        
    };
}
    if($window.location.pathname=="/MyExperience") {
    $scope.swipeLeft = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-left ng-enter');
        $location.path("/MyProjects");
    };
    $scope.swipeRight = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-right ng-enter');
        $location.path("/MySkills");        
    };
}
if($window.location.pathname=="/MyProjects") {
    $scope.swipeLeft = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-left ng-enter');
        $location.path("/MyCertifications");
    };
    $scope.swipeRight = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-right ng-enter');
        $location.path("/MyExperience");        
    };
}
if($window.location.pathname=="/MyCertifications") {
    $scope.swipeLeft = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-left ng-enter');
        $location.path("/ContactMe");
    };
    $scope.swipeRight = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-right ng-enter');
        $location.path("/MyProjects");        
    };
}
if($window.location.pathname=="/ContactMe") {
    $scope.swipeLeft = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-left ng-enter');
        $location.path("/");
    };
    $scope.swipeRight = function () {
        var myEl = angular.element( document.querySelector( '.vista' ) );
        myEl.addClass('swipe-right ng-enter');
        $location.path("/MyCertifications");        
    };
}
    }, 1000);

})

.controller('skills', function($scope, $http, $interval, $location, $window) {
    $interval(function () {
        $http.get('/v1/skills/').
        then(function(response) {
            $scope.skills = response.data;
        });
    }, 1000);
})

.controller('empresas', function($scope, $http, $interval, $location, $window) {
    $interval(function () {
    $http.get('/v1/empresas/').
        then(function(response) {
            $scope.empresas = response.data;
        });
    }, 1000);
})
.controller('certificaciones', function($scope, $http, $interval, $location, $window) {
    $http.get('/v1/certificaciones/').
        then(function(response) {
            $scope.certificaciones = response.data;
        });
})
.controller('trabajos', function($scope, $http, $interval, $location, $window) {
    $interval(function () {
    $http.get('/v1/trabajos/').
        then(function(response) {
            $scope.trabajos = response.data;
        });
    }, 1000);

})
.controller('contacto', function($scope, $http, $interval, $location, $window) {

});

app.config(function($routeProvider, $locationProvider,$provide) {
    $routeProvider
    .when("/", {
        templateUrl : "about_me.htm"
    })
    .when("/MySkills", {
        templateUrl : "my_skills.htm"
    })
    .when("/MyExperience", {
        templateUrl : "my_experience.htm"
    })
    .when("/MyProjects", {
        templateUrl : "my_projects.htm"
    })
    .when("/MyCertifications", {
        templateUrl : "my_certifications.htm"
    })
    .when("/ContactMe", {
        templateUrl : "contact_me.htm"
    });
    $locationProvider.html5Mode(true);  
});