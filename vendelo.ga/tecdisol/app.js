var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "start.html"
    })
    .when("/about", {
        templateUrl : "about.html"
    })
    .when("/services", {
        templateUrl : "services.html"
    });
});